import 'package:firstapp/detail/detail.dart';
import 'package:firstapp/home/home.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      // tabs navigation
      // home: DefaultTabController(
      //   length: 2,
      //   child: Scaffold(
      //     appBar: AppBar(
      //       bottom: TabBar(
      //         tabs: [
      //           Tab(icon: Icon(Icons.directions_car)),
      //           Tab(icon: Icon(Icons.directions_transit))
      //         ],
      //       ),
      //       title: Text('Flutter Tabs Example'),
      //     ),
      //     body: TabBarView(
      //       children: [
      //         MyHomePage(),
      //         DetailPage(),
      //       ],
      //     ),
      //   ),
      // )


      debugShowCheckedModeBanner: false, //for remove debug banner
      title: 'Murihat Flutter',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(),
      

      // Router
      // // initialRoute: '/',
      // // routes: {
      // //   // When navigating to the "/" route, build the FirstScreen widget.
      // //   '/': (context) =>  MyHomePage(title: 'Murihat Flutter'),
      // //   // When navigating to the "/second" route, build the SecondScreen widget.
      // //   '/detail': (context) => DetailPage(title: 'Detail Flutter'),
      // },
    );
  }
}