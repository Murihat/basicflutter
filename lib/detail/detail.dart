import 'package:flutter/material.dart';

class DetailPage extends StatefulWidget {
  DetailPage({Key key, this.title, this.todo}) : super(key: key);

  final String title;
  final String todo;

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Detail")),
        body: Container(
          color: Colors.grey,
          child: Center(
            child: Text("Detail Page", style: TextStyle(fontSize: 46))
          ),
        )
    );
  }
}