import 'package:firstapp/detail/detail.dart';
import 'package:firstapp/drawer/drawer.dart';
import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';


class MyHomePage extends StatefulWidget {
  // for parsing data
  // MyHomePage({Key key, this.title}) : super(key: key);
  // define type data 
  // final String title;
  
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>(); //global key for executed function or class

  void _showDialog(title, message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _snackBar(title, message){
    Flushbar flush;

    flush = Flushbar(
      title: title,
      message: message,
      backgroundColor: Colors.green,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      isDismissible: false,
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      icon: Icon(
        Icons.check,
        color: Colors.greenAccent,
      ),
      mainButton: FlatButton(
        child: Text(
          "Close",
          style: TextStyle(color: Colors.amber),
        ),
        onPressed: () { flush.dismiss(true); },
      ),
    )..show(context);
    // docs link : https://pub.dev/packages/flushbar#-readme-tab- 
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: openDrawer(),
      appBar: AppBar(title: Text("Home"), actions: <Widget>[
        IconButton(
          icon: Icon(Icons.notifications),
          onPressed: () {
            _showDialog("Yeay! Clicked", "Thanks Your For Click This Icon :)");
          },
        )
      ]),
      body: Container(
        color: Colors.grey,
        // height: 240.0,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Home Page', style: TextStyle(fontSize: 46)),
              RaisedButton(
                onPressed: () {
                  _scaffoldKey.currentState.openDrawer();
                },
                child: Text('Open Drawer', style: TextStyle(fontSize: 20)),
              ),
              RaisedButton(
                onPressed: () {
                  _snackBar("Yeay! Clicked snackBar", "Thanks you for try click button snackBar");
                },
                child: Text('Show SnackBar', style: TextStyle(fontSize: 20)),
              ),
              RaisedButton(
                color: Colors.blue, //specify background color for the button here
                colorBrightness: Brightness.dark, //specify the color brightness here, either `Brightness.dark` for darl and `Brightness.light` for light
                disabledColor:  Colors.blueGrey, // specify color when the button is disabled
                highlightColor: Colors.red, //color when the button is being actively pressed, quickly fills the button and fades out after
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 5.0),
                onPressed: () {
                  var route = new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new DetailPage(),
                  );
                  Navigator.of(context).push(route);
                    // Navigator.pushNamed(context, '/detail');
                  // _showDialog("Yeay! Clicked",
                  //     "Waiting Function navigation go to detail");
                },
                child: Text('Go To Detail', style: TextStyle(fontSize: 20)),
              ),
            ],
          ),
        )
      ),
    );
  }
}
