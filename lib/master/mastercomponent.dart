import 'package:firstapp/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';

class MasterComponent{

  void _snackBar(title, message, context){
    Flushbar flush;
    flush = Flushbar(
      title: title,
      message: message,
      backgroundColor: Colors.grey,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
      isDismissible: false,
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      icon: Icon(
        Icons.check,
        color: Colors.greenAccent,
      ),
      mainButton: FlatButton(
        child: Text(
          "Done",
          style: TextStyle(color: Colors.amber),
        ),
        onPressed: () { flush.dismiss(true); },
      ),
    )..show(context);
    // docs link : https://pub.dev/packages/flushbar#-readme-tab- 
  }
}

  
  

