import 'package:flutter/material.dart';

class openDrawer extends StatefulWidget {
  @override
  _openDrawerState createState() => _openDrawerState();
}

class _openDrawerState extends State<openDrawer> {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              'Murihat Drawer',
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w900,
                fontFamily: "Georgia",
                color: Colors.white
              ),
            ),
            decoration: BoxDecoration(
              color: Colors.red,
            ),
          ),
          ListTile(
            title: Text('Item 1'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Item 2'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
